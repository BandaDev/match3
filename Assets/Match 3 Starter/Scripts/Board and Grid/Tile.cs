﻿/*
 * Copyright (c) 2017 Razeware LLC
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using System;

public class Tile : MonoBehaviour
{
    public GameObject deadAnim;
    public GameObject explosionEffect;
    private static Color selectedColor = new Color(.5f, .5f, .5f, 1.0f);
    private static Tile previousSelected = null;
    private SpriteRenderer render;
    private bool isSelected = false;
    private bool matchFound = false;
    private static bool _matchFound = false;
    //public bool powerUp { get; set; }
    private Vector2[] adjacentDirections = new Vector2[] { Vector2.up, Vector2.down, Vector2.left, Vector2.right };
    private static bool animOn = false;

    void Awake()
    {
        render = GetComponent<SpriteRenderer>();
    }

    private void Select()
    {
        isSelected = true;
        render.color = selectedColor;
        previousSelected = gameObject.GetComponent<Tile>();
        SFXManager.instance.PlaySFX(Clip.Select);
    }

    private void Deselect()
    {
        isSelected = false;
        render.color = Color.white;
        previousSelected = null;
    }
    public bool getMatchStat()
    {
        return matchFound;
    }

    void OnMouseDown()
    {
        //BoardManager.instance.findPossMove();
        // 1
        if (render.sprite == null || BoardManager.instance.IsShifting || GameManager.instance.gameOver)
        {
            return;
        }

        if (isSelected)
        { // 2 Is it already selected?
            Deselect();
        }
        else
        {
            if (previousSelected == null)
            { // 3 Is it the first tile selected?
                Select();
            }
            else
            {
                if (GetAllAdjacentTiles().Contains(previousSelected.gameObject))
                { // 1
                    StartCoroutine(MoveState(previousSelected));

                }
                else
                { // 3

                    previousSelected.GetComponent<Tile>().Deselect();

                    Select();
                }
            }

        }
    }

    private IEnumerator MoveState(Tile T)
    {
        _matchFound = false;
        //BoardManager.instance.animOn = true;
        previousSelected.Deselect();
        yield return SwapSpriteAnim(T);
        //SwapSprite(previousSelected.render);
        //previousSelected.ClearAllMatches();
        //ClearAllMatches();
        //StopCoroutine(BoardManager.instance.FindNullTiles());
        yield return BoardManager.instance.FindNullTiles();
        //yield return new WaitForSeconds(0.1f);
        if (!_matchFound)
        {
            //BoardManager.instance.animOn = true;
            //StopCoroutine(SwapSpriteAnim(previousSelected));
            SFXManager.instance.PlaySFX(Clip.Error);
            yield return StartCoroutine(SwapSpriteAnim(T));
            //SwapSprite(previousSelected.render);

            Debug.Log("mossa non valida");
            GUIManager.instance.UnFreeze();
        }
        else
        {
            yield return new WaitUntil(() => BoardManager.instance.IsShifting == false);

        }

    }

    public void SwapSprite(SpriteRenderer render2)
    { // 1
        //new WaitForSeconds(0.8f);
        if (render.sprite == render2.sprite)
        { // 2
            return;
        }
        Sprite tempSprite = render2.sprite; // 3
        render2.sprite = render.sprite; // 4
        render.sprite = tempSprite; // 5
        //GUIManager.instance.MoveCounter--;
        //SFXManager.instance.PlaySFX(Clip.Swap); // 6
    }

    private IEnumerator SwapSpriteAnim(Tile T2)
    { // 1
      //new WaitForSeconds(0.8f);
      //
      //render.enabled = false;
      //T2.render.enabled = false;
        yield return new WaitForSeconds(0.1f);
        yield return new WaitUntil(() => !animOn);

        //Debug.Log("inizio anim");


        animOn = true;
        Vector3 target1 = T2.transform.position;
        Vector3 target2 = transform.position;
        Vector3 start1 = transform.position;
        Vector3 start2 = T2.transform.position;
        float i = 0;
        while (transform.position != target1)
        {
            transform.position = Vector3.Lerp(start1, target1, i);
            T2.transform.position = Vector3.Lerp(start2, target2, i);
            yield return new WaitForSeconds(0.01f);
            i += 0.1f;
        }

        transform.position = start1;
        T2.transform.position = start2;

        SwapSprite(T2.render);

        //render.enabled = true;
        //T2.render.enabled = true;

        SFXManager.instance.PlaySFX(Clip.Swap); // 6
        animOn = false;
        yield break;
    }
    public void fakeSwapSprite(SpriteRenderer render2)
    { // 1
        //new WaitForSeconds(0.8f);
        if (render.sprite == render2.sprite)
        { // 2
            return;
        }

        Sprite tempSprite = render2.sprite; // 3
        render2.sprite = render.sprite; // 4
        render.sprite = tempSprite; // 5
        //GUIManager.instance.MoveCounter--;
        //SFXManager.instance.PlaySFX(Clip.Swap); // 6
    }

    private GameObject GetAdjacent(Vector2 castDir)
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, castDir);
        if (hit.collider != null)
        {
            return hit.collider.gameObject;
        }
        return null;
    }

    public List<GameObject> GetAllAdjacentTiles()
    {
        List<GameObject> adjacentTiles = new List<GameObject>();
        for (int i = 0; i < adjacentDirections.Length; i++)
        {
            if (GetAdjacent(adjacentDirections[i]) != null)
                adjacentTiles.Add(GetAdjacent(adjacentDirections[i]));
        }
        return adjacentTiles;
    }

    private List<GameObject> FindMatch(Vector2 castDir)
    { // 1
        List<GameObject> matchingTiles = new List<GameObject>(); // 2
        RaycastHit2D hit = Physics2D.Raycast(transform.position, castDir); // 3
        while (hit.collider != null && hit.collider.GetComponent<SpriteRenderer>().sprite == render.sprite)
        { // 4
            matchingTiles.Add(hit.collider.gameObject);
            hit = Physics2D.Raycast(hit.collider.transform.position, castDir);
        }
        return matchingTiles; // 5
    }

    private List<GameObject> GetMatch(Vector2[] paths) // 1
    {
        if (this.render.sprite == null)
        {
            return null;
        }
        List<GameObject> matchingTiles = new List<GameObject>(); // 2
        for (int i = 0; i < paths.Length; i++) // 3
        {
            matchingTiles.AddRange(FindMatch(paths[i]));
        }
        if (matchingTiles.Count >= 2) // 4
        {

            matchFound = true; // 6
            matchingTiles.Add(this.gameObject);
            return matchingTiles;
        }
        return new List<GameObject>();
    }

    public void ClearAllMatches()
    {
        List<GameObject> matchingTiles = new List<GameObject>();
        if (render.sprite == null)
            return;

        matchingTiles.AddRange(GetMatch(new Vector2[2] { Vector2.left, Vector2.right }));
        matchingTiles.AddRange(GetMatch(new Vector2[2] { Vector2.up, Vector2.down }));
        /*if (previousSelected != null)
        {
            matchingTiles.AddRange(previousSelected.GetMatch(new Vector2[2] { Vector2.left, Vector2.right }));
            matchingTiles.AddRange(previousSelected.GetMatch(new Vector2[2] { Vector2.up, Vector2.down }));
        }*/


        if (matchFound)
        {
            _matchFound = true;
            bool power = false;
            foreach (GameObject temp in matchingTiles)
            {
                if (temp.GetComponent<Tile>().GetTileSprite() == BoardManager.instance.powerUp[BoardManager.instance.choosePowerUp])
                {
                    power = true;
                    break;
                }
            }
            if (power)
            {
                if (BoardManager.instance.choosePowerUp == 0)
                {
                    List<GameObject> exploseTiles = new List<GameObject>();
                    for (int i = 0; i < matchingTiles.Count; i++)
                    {
                        if (BoardManager.instance.powerUp[0] == matchingTiles[i].GetComponent<SpriteRenderer>().sprite)
                        {
                            exploseTiles.AddRange(matchingTiles[i].GetComponent<Tile>().GetAllAdjacentTiles());
                        }
                    }
                    Debug.Log("Bomba!");
                    foreach (GameObject temp in exploseTiles)
                    {
                        temp.GetComponent<Tile>().istanziateExplosion();
                    }
                    //matchingTiles.AddRange(this.GetAllAdjacentTiles());
                    matchingTiles.AddRange(exploseTiles);
                    //StopCoroutine(BoardManager.instance.TogglePowerUp());
                    //StartCoroutine(BoardManager.instance.TogglePowerUp());
                }
                else if (BoardManager.instance.choosePowerUp == 1)
                {
                    Debug.Log("za warudo");
                    GUIManager.instance.Freeze();

                }
            }
            else
            {
                for (int i = 0; i < matchingTiles.Count; i++)
                {
                    matchingTiles[i].GetComponent<Tile>().DeadPart();
                }

            }

            //render.sprite = null;
            for (int i = 0; i < matchingTiles.Count; i++)
            {
                matchingTiles[i].GetComponent<SpriteRenderer>().sprite = null;
            }

            matchFound = false;

            //StopCoroutine(BoardManager.instance.FindNullTiles());
            //StartCoroutine(BoardManager.instance.FindNullTiles());

            //new WaitUntil(() => !BoardManager.instance.IsShifting);
            //Debug.Log("shifting finito");
            //BoardManager.instance.findPossMove();

        }
    }

    private void DeadPart()
    {
        Instantiate(deadAnim, this.transform);
    }

    private Sprite GetTileSprite()
    {
        return render.sprite;
    }

    private void istanziateExplosion()
    {
        Instantiate(explosionEffect, this.transform);
    }

    public bool IsValidMove(Tile T)
    {

        fakeSwapSprite(T.GetComponent<SpriteRenderer>());
        GetMatch(new Vector2[2] { Vector2.left, Vector2.right });
        GetMatch(new Vector2[2] { Vector2.up, Vector2.down });
        T.GetMatch(new Vector2[2] { Vector2.left, Vector2.right });
        T.GetMatch(new Vector2[2] { Vector2.up, Vector2.down });

        fakeSwapSprite(T.GetComponent<SpriteRenderer>());
        if (matchFound || T.matchFound)
        {
            matchFound = false;
            T.matchFound = false;
            Debug.Log("ci sono mosse disp");
            return true;
        }
        return false;
    }

}