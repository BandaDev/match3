﻿/*
 * Copyright (c) 2017 Razeware LLC
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BoardManager : MonoBehaviour
{

    public static BoardManager instance;
    public List<Sprite> characters = new List<Sprite>();
    public List<Sprite> powerUp = new List<Sprite>();
    //public GameManager manager;
    public GameObject tile;
    //public DataManager Data;
    public int xSize, ySize;
    public bool animOn = false;
    public int choosePowerUp { get; set; }

    private GameObject[,] tiles;
    private int bonus;

    public bool IsShifting { get; set; }
    public bool PowerUpTrigger { get; set; }
    void Start()
    {
        instance = GetComponent<BoardManager>();

        Vector2 offset = tile.GetComponent<SpriteRenderer>().bounds.size;
        CreateBoard(offset.x, offset.y);
    }

    private void CreateBoard(float xOffset, float yOffset)
    {
        GameManager.instance.gameOver = false;
        bonus = 1;
        choosePowerUp = PlayerPrefs.GetInt("POWERUP");

        tiles = new GameObject[xSize, ySize];

        float startX = transform.position.x;
        float startY = transform.position.y;
        Sprite[] previousLeft = new Sprite[ySize];
        Sprite previousBelow = null;
        for (int x = 0; x < xSize; x++)
        {
            for (int y = 0; y < ySize; y++)
            {
                GameObject newTile = Instantiate(tile, new Vector3(startX + (xOffset * x), startY + (yOffset * y), 0), tile.transform.rotation);
                tiles[x, y] = newTile;
                newTile.transform.parent = transform; // 1

                List<Sprite> possibleCharacters = new List<Sprite>(); // 1
                possibleCharacters.AddRange(characters); // 2
                possibleCharacters.Add(powerUp[choosePowerUp]);

                possibleCharacters.Remove(previousLeft[y]); // 3
                possibleCharacters.Remove(previousBelow);

                Sprite newSprite = possibleCharacters[UnityEngine.Random.Range(0, possibleCharacters.Count)];

                newTile.GetComponent<SpriteRenderer>().sprite = newSprite; // 3
                previousLeft[y] = newSprite;
                previousBelow = newSprite;
                //Debug.Log(possibleCharacters.Count);
            }
        }
        FindPossMove();
    }
    public IEnumerator FindNullTiles()
    {
        //TogglePowerUp();

        bool nullPresent = false;
        yield return new WaitForSeconds(.1f);
        ClearTiles();

        //yield return new WaitUntil(() => animOn == false);
        for (int x = 0; x < xSize; x++)
        {
            for (int y = 0; y < ySize; y++)
            {
                if (tiles[x, y].GetComponent<SpriteRenderer>().sprite == null)
                {
                    yield return StartCoroutine(ShiftTilesDown(x, y));
                    nullPresent = true;
                    break;
                }
            }
        }
        if (nullPresent)
        {
            if (bonus >= 2)
            {
                Debug.Log("combo X" + bonus);
                GUIManager.instance.Combo(bonus);
            }
            bonus++;
            yield return FindNullTiles();
            yield break;
        }
        else
        {
            Debug.Log("reset bonus");
            bonus = 1;
        }
        FindPossMove();
    }

    private void ClearTiles()
    {
        for (int x = 0; x < xSize; x++)
        {
            for (int y = 0; y < ySize; y++)
            {
                tiles[x, y].GetComponent<Tile>().ClearAllMatches();
            }
        }

    }

    private IEnumerator ShiftTilesDown(int x, int yStart, float shiftDelay = .1f)
    {
        IsShifting = true;
        List<SpriteRenderer> renders = new List<SpriteRenderer>();
        int nullCount = 0;
        SpriteRenderer render;
        for (int y = yStart; y < ySize; y++)
        {  // 1
            render = tiles[x, y].GetComponent<SpriteRenderer>();
            if (render.sprite == null)
            { // 2
                nullCount++;

            }
            renders.Add(render);
        }
        for (int i = 0; i < nullCount; i++)
        { // 3

            GUIManager.instance.Score += 50 * bonus;
            yield return new WaitForSeconds(shiftDelay);// 4

            if (renders.Count == 1)
            {
                renders[0].sprite = GetNewSprite(x, ySize - 1);
            }
            else
            {
                Sprite temp;
                if (renders[0].sprite != null)
                {
                    renders.RemoveAt(0);
                }
                for (int k = 0; k < renders.Count - 1; k++)
                { // 5
                    if (renders[k].sprite == null)
                    {
                        renders[k].sprite = GetNewSprite(x, ySize - 1);
                    }
                    temp = renders[k].sprite;
                    renders[k].sprite = renders[k + 1].sprite;
                    renders[k + 1].sprite = temp;


                }
                yield return new WaitForSeconds(shiftDelay);// 
            }
        }


        IsShifting = false;
    }
    private Sprite GetNewSprite(int x, int y)
    {
        int select;
        List<Sprite> possibleCharacters = new List<Sprite>();

        possibleCharacters.AddRange(characters);
        possibleCharacters.Add(powerUp[choosePowerUp]);
        //possibleCharacters.AddRange();

        if (x > 0)
        {
            possibleCharacters.Remove(tiles[x - 1, y].GetComponent<SpriteRenderer>().sprite);
        }
        if (x < xSize - 1)
        {
            possibleCharacters.Remove(tiles[x + 1, y].GetComponent<SpriteRenderer>().sprite);
        }
        if (y > 0)
        {
            possibleCharacters.Remove(tiles[x, y - 1].GetComponent<SpriteRenderer>().sprite);
        }

        select = UnityEngine.Random.Range(0, possibleCharacters.Count);

        return possibleCharacters[select];
    }

    public void FindPossMove()
    {
        // search for move
        for (int x = 0; x < xSize - 1; x++)
        {
            for (int y = 0; y < ySize; y++)
            {
                if (tiles[x, y].GetComponent<Tile>().IsValidMove(tiles[x + 1, y].GetComponent<Tile>()))

                {
                    Debug.Log("mossa disp a " + x + " " + y);
                    return;
                }
            }
        }
        for (int x = 0; x < xSize; x++)
        {
            for (int y = 0; y < ySize - 1; y++)
            {
                if (tiles[x, y].GetComponent<Tile>().IsValidMove(tiles[x, y + 1].GetComponent<Tile>()))

                {
                    Debug.Log("mossa disp a " + x + " " + y);
                    return;
                }
            }
        }
        Debug.Log("game Over");
        GameManager.instance.GameOver();

    }
}