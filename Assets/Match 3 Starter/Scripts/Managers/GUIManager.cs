﻿/*
 * Copyright (c) 2017 Razeware LLC
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GUIManager : MonoBehaviour
{
    public static GUIManager instance;
    public GameObject ComboPart;
    public GameObject ComboPannel;
    public GameObject gameOverPanel;
    public Text yourScoreTxt;
    public Text highScoreTxt;
    public float totalTime = 120f; //2 minutes
    public Text scoreTxt;
    public Text timerTxt;
    public Text comboTxt;


    private int score;
    private int moveCounter;
    private bool freeze=false;

    public int Score
    {
        get
        {
            return score;
        }

        set
        {
            score = value;
            scoreTxt.text = score.ToString();
        }
    }

    void Awake()
    {
        instance = GetComponent<GUIManager>();
    }

    // Show the game over panel

    public void GameOverPannel()
    {
        Freeze();
        gameOverPanel.SetActive(true);
        //GameManager.instance.gameOver = true;

        if (score > PlayerPrefs.GetInt("HighScore"))
        {
            PlayerPrefs.SetInt("HighScore", score);
            highScoreTxt.text = "New Best: " + PlayerPrefs.GetInt("HighScore").ToString();
        }
        else
        {
            highScoreTxt.text = "Best: " + PlayerPrefs.GetInt("HighScore").ToString();
        }

        yourScoreTxt.text = score.ToString();
    }





    private void Update()
    {
        if (!freeze)
            totalTime -= Time.deltaTime;
        if (totalTime <= 0)
        {
            GameManager.instance.GameOver();
        }
        else
        {
            UpdateLevelTimer(totalTime);
        }
    }

    public void UpdateLevelTimer(float totalSeconds)
    {
        int minutes = Mathf.FloorToInt(totalSeconds / 60f);
        int seconds = Mathf.RoundToInt(totalSeconds % 60f);

        string formatedSeconds = seconds.ToString();

        if (seconds == 60)
        {
            seconds = 0;
            minutes += 1;
        }

        timerTxt.text = minutes.ToString("00") + ":" + seconds.ToString("00");
    }
    public void Freeze()
    {
        freeze = true;
    }
    public void UnFreeze()
    {
        freeze = false;
    }
    public void Combo(int bonus)
    {
        {
        Instantiate(ComboPart, ComboPannel.transform);
        }
        StopCoroutine(ComboCounter(bonus));
        StartCoroutine(ComboCounter(bonus));
    }
    public IEnumerator ComboCounter(int bonus)
    {
        comboTxt.text = "X" + bonus;
        ComboPannel.SetActive(true);
        //if (bonus >= 2)
        SFXManager.instance.PlaySFX((Clip)bonus+2);
        yield return new WaitForSeconds(0.9f);
        ComboPannel.SetActive(false);

        yield break;
    }
}
